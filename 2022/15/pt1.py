# #################################
# Day #
# 
# #################################


def start(instructions):
    for i in instructions:
        print(i)
        

def formatInput(rows):
    fPuzzleInput = []

    for row in rows:
        fPuzzleInput.append(row)
    return fPuzzleInput


def main():
    input = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    fInput = formatInput(input)
    start(fInput)


if __name__ == '__main__':
    main()