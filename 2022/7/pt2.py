# #################################
# Day 7
# 
# #################################

def start(lines):
    """
    {
        /           : {total: 10000, a.txt: 50, b.log: 20},
        /dir1       : {total: 10000, x.out: 10, c.log: 20},
        /dir2/dir21 : {total: 10000, a.txt: 33, d.log: 20},
        ...
    }
    """
    # lines is a list of lists
    #   [ [a,b,c], [a,b,c,d], ... ]
    # each element of lines is a line from the terminal
    # each line from the terminal may describe either a command or information aobut 
    #   directory structure.

    # paths are key, size is value
    # e.g.  
    #   /a/b : 123
    paths = {}
    # cwd is a list of directories. Reading the elements in order create a dir path.
    cwd = ['/']

    for l in lines:
        if l[0] == '$':
            # This is a command

            if l[1] == 'cd':                
                if l[2] == '..':
                    # cwd up a level
                    cwd.pop()
                else:
                    # cwd down into dir
                    cwd.append(f'{l[2]}/')
            
            elif l[1] == 'ls':
                pass # do nothing
            else:
                print(f'[error] Unexpected command {l}')
        else:
            # This is not a command

            if l[0] == 'dir':   
                # if this dir does not already exist, make it
                key = ''.join(cwd)
                key += f'{l[1]}/'
                if key not in paths:
                    paths[key] = {'total': 0}
            elif l[0].isnumeric():
                key = ''.join(cwd)
                
                # if the key does not already exist, make it
                if key not in paths:
                    paths[key] = {'total': 0}
                
                # we are looking at a file, add the file size and name at this dir/key
                paths[key][l[1]] = int(l[0])
    
    x = [splitretain(k, '/') for k in paths.keys()]    
    sortedPathLists = sorted(x, key=len, reverse=False)
    #print(x)

    # paths
    # /:{'total': 0, 'b.txt': 14848514, 'c.dat': 8504156}
    # /a/:{'total': 0, 'f': 29116, 'g': 2557, 'h.lst': 62596}
    # /a/e/:{'total': 0, 'i': 584}
    # /d/:{'total': 0, 'j': 4060174, 'd.log': 8033020, 'd.ext': 5626152, 'k': 7214296}

    # sorted path lists
    # [['/', 'a/', 'e/'], ['/', 'a/'], ['/', 'd/'], ['/']]

    while len(sortedPathLists) > 0:
        pathlist = sortedPathLists.pop()

        if pathlist in sortedPathLists:
            continue

        while len(pathlist) > 0:
            if pathlist in sortedPathLists:
                break

            child_dir = ''.join(pathlist)
            child_val = 0
            
            # Get all values from this dir
            for k in paths[child_dir]:
                if k != 'total':
                    child_val += int(paths[child_dir][k])
            
            paths[child_dir]['total'] += child_val
            
            all_i_got_for_ya_bub = paths[child_dir]['total']

            # Add these values to the child's parent 
            parent_dir = ''.join(pathlist[:-1])
            if parent_dir in paths:
                paths[parent_dir]['total'] += all_i_got_for_ya_bub

            pathlist.pop()

    running_total = 0
    for p in paths:
        if paths[p]['total'] <= 100000:
            running_total += paths[p]['total']
    print(f'/ : {paths["/"]}')
    
    # Smallest file greater than: 1735494

    print('---------------')

    path_del_candidates = []
    for x in paths:
        total = paths[x]['total']
        if total > 1735494:
            path_del_candidates.append({'path':x, 'total':total})

    sortedpaths = sorted(path_del_candidates, key=lambda k: k['total'])
    for s in sortedpaths:
        print(s)

    # {'path': '/nzwbc/zcv/nbq/', 'total': 1815525}



def splitretain(somestring, char):
    """ Split string, but retain the trailing character.
        If the first character is the matching character, returns a split with just the matching character.
        If the last character is not a matching character, the remainder of the buffer is returned as if it was split.
    """
    retval = []
    buff = ''
    for c in somestring:
        if c == char:
            buff += char 
            retval.append(buff)
            buff = ''
        else:
            buff += c
    
    if len(buff) > 0:
        retval.append(buff)
    
    return retval


def formatInput(data):
    fData = []

    for i in data:
        lines = i.split()
        fData.append(lines)
    return fData


def main():
    data = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            data.append(r)
    fData = formatInput(data)
    start(fData)


if __name__ == '__main__':
    main()