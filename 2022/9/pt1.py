# #################################
# Day 9
# Plancks
# #################################


def start(instructions):
    head = Head()
    tail = Tail()
    unique_positions = set()
    unique_positions.add(tail.current_position())
    
    for inst in instructions:
        direction = inst[0]
        magnitude = inst[1]
        head.update_position(direction, magnitude)
        tail_trail = tail.update_position(head.x, head.y)
        for t in tail_trail:
            unique_positions.add(t)
    
    #print(unique_positions)
    print(f'Solution: {len(unique_positions)}')


class Knot:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def current_position(self) -> str: 
        return f'{self.x}:{self.y}'


class Head(Knot):
    def __init__(self):
        Knot.__init__(self)
        
    def update_position(self, direction, magnitude) -> None: 
        if direction == 'U':
            self.y += magnitude
        elif direction == 'D':
            self.y -= magnitude
        elif direction == 'R':
            self.x += magnitude
        elif direction == 'L':
            self.x -= magnitude
        else:
            print(f'[ERROR] Unrecognized Instrcution: {direction} {magnitude}')


class Tail(Knot):
    def __init__(self):
        Knot.__init__(self)
        
    def update_position(self, head_x, head_y) -> list: 
        """ Update tail position relative to the Head coordinates.
            The tail takes the shortest path to the provided coordinates.
            If the tail is already touching or adjacent, no move is required. 
        """
        x_diff = head_x - self.x
        y_diff = head_y - self.y
        tail_trail = []

        if abs(x_diff) <= 1 and abs(y_diff) <= 1:
            # Nothing to do
            return tail_trail

        if abs(x_diff) < abs(y_diff):
            # Tail is closer on x-dim; Head moved vertically.
            self.x = head_x

            while y_diff > 1:
                self.y += 1
                y_diff = head_y - self.y
                tail_trail.append(self.current_position())
            
            while y_diff < -1:
                self.y -= 1
                y_diff = head_y - self.y
                tail_trail.append(self.current_position())
        
        if abs(y_diff) < abs(x_diff):
            # Tail is closer on x-dim; Head moved vertically.
            self.y = head_y

            while x_diff > 1:
                self.x += 1
                x_diff = head_x - self.x
                tail_trail.append(self.current_position())
            
            while x_diff < -1:
                self.x -= 1
                x_diff = head_x - self.x
                tail_trail.append(self.current_position())
        
        return tail_trail


def formatInput(rows):
    fPuzzleInput = []

    for row in rows:
        commands = row.split()
        direction = commands[0]
        magnitude = int(commands[1])
        fPuzzleInput.append([direction, magnitude])
    return fPuzzleInput


def main():
    input = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    fInput = formatInput(input)
    start(fInput)


if __name__ == '__main__':
    main()