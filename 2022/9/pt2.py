# #################################
# Day 9
# Plancks
# 10 knots 
# They need to incrementally follow each other, unlike pt1 where we could get away with tracing a single full path.
# #################################


def start(instructions):
    head = Head()
    tail1 = Tail()
    tail2 = Tail()
    tail3 = Tail()
    tail4 = Tail()
    tail5 = Tail()
    tail6 = Tail()
    tail7 = Tail()
    tail8 = Tail()
    tail9 = Tail()
    unique_positions = set()
    unique_positions.add(tail9.current_position())
    
    for inst in instructions:
        direction = inst[0]
        magnitude = inst[1]
        head.update_position(direction, magnitude)
        tail1.update_position(head.x, head.y)
        tail2.update_position(tail1.x, tail1.y)
        tail3.update_position(tail2.x, tail2.y)
        tail4.update_position(tail3.x, tail3.y)
        tail5.update_position(tail4.x, tail4.y)
        tail6.update_position(tail5.x, tail5.y)
        tail7.update_position(tail6.x, tail6.y)
        tail8.update_position(tail7.x, tail7.y)
        if tail8.x == 5:
            True
        tail_trail = tail9.update_position(tail8.x, tail8.y)
        for t in tail_trail:
            unique_positions.add(t)
            print(t)
    
    #print(unique_positions)
    print(f'Solution: {len(unique_positions)}')


class Knot:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def current_position(self) -> str: 
        return f'{self.x}:{self.y}'


class Head(Knot):
    def __init__(self):
        Knot.__init__(self)
        
    def update_position(self, direction, magnitude) -> None: 
        if direction == 'U':
            self.y += magnitude
        elif direction == 'D':
            self.y -= magnitude
        elif direction == 'R':
            self.x += magnitude
        elif direction == 'L':
            self.x -= magnitude
        else:
            print(f'[ERROR] Unrecognized Instrcution: {direction} {magnitude}')


class Tail(Knot):
    def __init__(self):
        Knot.__init__(self)
        
    def update_position(self, head_x, head_y) -> list: 
        """ Update tail position relative to the Head coordinates.
            The tail takes the shortest path to the provided coordinates.
            If the tail is already touching or adjacent, no move is required. 
        """
        x_diff = head_x - self.x
        y_diff = head_y - self.y
        tail_trail = []

        if abs(x_diff) <= 1 and abs(y_diff) <= 1:
            # Nothing to do
            return tail_trail

        if abs(x_diff) < abs(y_diff):
            # Tail is closer on x-dim; Head moved vertically.
            self.x = head_x

            while y_diff > 1:
                self.y += 1
                y_diff = head_y - self.y
                tail_trail.append(self.current_position())
            
            while y_diff < -1:
                self.y -= 1
                y_diff = head_y - self.y
                tail_trail.append(self.current_position())
        
        if abs(y_diff) < abs(x_diff):
            # Tail is closer on x-dim; Head moved vertically.
            self.y = head_y

            while x_diff > 1:
                self.x += 1
                x_diff = head_x - self.x
                tail_trail.append(self.current_position())
            
            while x_diff < -1:
                self.x -= 1
                x_diff = head_x - self.x
                tail_trail.append(self.current_position())
        
        return tail_trail


def formatInput(rows):
    fPuzzleInput = []

    for row in rows:
        commands = row.split()
        direction = commands[0]
        magnitude = int(commands[1])
        fPuzzleInput.append([direction, magnitude])
    return fPuzzleInput


def main():
    input = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    fInput = formatInput(input)
    start(fInput)


if __name__ == '__main__':
    main()