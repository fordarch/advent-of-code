# #################################
# Day #
# 
# #################################


def start(lines):
    print('')

def formatInput(input):
    fInput = []

    for i in input:
        fInput.append(i)
    return fInput


def main():
    input = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    fInput = formatInput(input)
    start(fInput)


if __name__ == '__main__':
    main()