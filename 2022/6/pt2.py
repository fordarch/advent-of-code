# #################################
# Day 6
# signal
# #################################

def start(data):
    counter = 14
    idx_start = 0
    idx_end = 14
    s = set()

    for _ in range(len(data)):
        buffer = data[idx_start:idx_end]

        for b in buffer:
            s.add(b)

        if len(s) == 14:
            print(f'Counter: {counter}')
            break
        else:
            counter += 1
            idx_start += 1
            idx_end += 1
            s = set()




def formatInput(data):
    fData = []
    
    fData = [char for char in data[0]]
    return fData


def main():
    data = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            data.append(r)
    fData = formatInput(data)
    start(fData)


if __name__ == '__main__':
    main()