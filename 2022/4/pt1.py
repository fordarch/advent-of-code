# #################################
# Day #
# 
# #################################


def start(data):
    total = 0
    for d in data:
        left = d[0].split('-')
        right = d[1].split('-')

        left[0] = int(left[0])
        left[1] = int(left[1])
        right[0] = int(right[0]) 
        right[1] = int(right[1])
        
        if left[0] <= right[0] and left[1] >= right[1]:
            total += 1   
        elif right[0] <= left[0] and right[1] >= left[1]:
            total += 1

    print(total)


def formatInput(data):
    fData = []

    for i in data:
        t = i.split(',')        
        fData.append(t)

    return fData


def main():
    data = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            data.append(r)
    fData = formatInput(data)
    start(fData)


if __name__ == '__main__':
    main()