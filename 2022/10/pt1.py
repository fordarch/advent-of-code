# #################################
# Day 10
# 
# #################################


def start(instructions):
    cycle_count = 1
    register_value = 1
    interesting_signal_strengths = []
    interesting_signal_cycles = [20, 60, 100, 140, 180, 220]

    for inst in instructions:
        instruction_duration = durationFromInstruction(inst[0])
        for _ in range(instruction_duration):
            if cycle_count in interesting_signal_cycles:
                interesting_signal_strengths.append(register_value * cycle_count)
            cycle_count += 1
        register_value += inst[1]
        
    answer = sum(interesting_signal_strengths)
    print(f'Answer: {answer}')


def durationFromInstruction(inst):
    if inst == 'noop':
        return 1
    elif inst == 'addx':
        return 2
    else:
        print(f'[ERROR] Unrecognized instrcution: {inst}')
        return None


def formatInput(rows):
    fPuzzleInput = []

    for row in rows:
        instruction = row.split()
        inst = instruction[0]
        inst_val = int(instruction[1]) if len(instruction) > 1 else 0
        fPuzzleInput.append([inst, inst_val])
    return fPuzzleInput


def main():
    input = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    fInput = formatInput(input)
    start(fInput)


if __name__ == '__main__':
    main()