# #################################
# Day 10
# CRT
# #################################


def start(instructions):
    cycle_count = 1
    interesting_signal_cycles = [40, 80, 120, 160, 200, 240]

    sprite_position = [0,1,2]
    crt_row = ['.']*40
    crt_screen = []
    pixle_position = 0

    for inst in instructions:
        instruction_duration = durationFromInstruction(inst[0])
        flag = False
        for _ in range(instruction_duration):
            if pixle_position in sprite_position:
                crt_row[pixle_position] = '#'            

            pixle_position += 1 

            if cycle_count in interesting_signal_cycles: # set to 40
                crt_screen.append(crt_row)  
                pixle_position = 0          
                crt_row = ['.']*40  

            cycle_count += 1
        
        sprite_position = [v + inst[1] for v in sprite_position]

    for row in crt_screen:
        print(''.join(row))
        
        


def durationFromInstruction(inst):
    if inst == 'noop':
        return 1
    elif inst == 'addx':
        return 2
    else:
        print(f'[ERROR] Unrecognized instrcution: {inst}')
        return None


def formatInput(rows):
    fPuzzleInput = []

    for row in rows:
        instruction = row.split()
        inst = instruction[0]
        inst_val = int(instruction[1]) if len(instruction) > 1 else 0
        fPuzzleInput.append([inst, inst_val])
    return fPuzzleInput


def main():
    input = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    fInput = formatInput(input)
    start(fInput)


if __name__ == '__main__':
    main()