# #################################
# Day 8
# 
# #################################


def start(data):
    printdata(data)

    x = 0
    y = 0

    # [y, x]
    # print(data[0][0]) # "top left"
    # print(data[0][4]) # "top right"
    # print(data[4][0]) # "bottom left"
    # print(data[4][4]) # "bottom right"

    tree_count = 0
    max_y = len(data)
    max_x = len(data[0])

    # data is square, dawg
    for y in range(max_y):
        for x in range(max_x):
            #print(f'POS:(y:{y},x:{x}) -- val: {data[y][x]}')

            # Check that vertical, up and down
            if y == 0 or y == max_y:
                tree_count += 1
                #print('Clearing Found! -- Edge')
                continue

            upper_list = [u[x] for u in data[:y+1]]
            thereIsAClearingToTheNorth = lastElementBiggestElement(upper_list)

            if thereIsAClearingToTheNorth:
                tree_count += 1
                #print('Clearing Found! -- North')
                continue

            lower_list = [d[x] for d in data[y:]]
            lower_list.reverse()
            thereIsAClearingToTheSouth = lastElementBiggestElement(lower_list)

            if thereIsAClearingToTheSouth:
                tree_count += 1
                #print('Clearing Found! -- South')
                continue

            # Check those sides, left and right
            if x == 0 or x == max_x:
                tree_count += 1
                #print('Clearing Found! -- Edge')
                continue

            left_list = [l for l in data[y][:x+1]]
            thereIsAClearingToTheWest = lastElementBiggestElement(left_list)

            if thereIsAClearingToTheWest:
                tree_count += 1
                #print('Clearing Found! -- West')
                continue

            right_list = [r for r in data[y][x:]]
            right_list.reverse()
            thereIsAClearingToTheEast = lastElementBiggestElement(right_list)

            if thereIsAClearingToTheEast:
                tree_count += 1
                #print('Clearing Found! -- East')
                continue

    print(tree_count)


def lastElementBiggestElement(somelist):
    last_element = somelist[-1]
    for elem in somelist[:-1]:
        if elem >= last_element:
            return False
    return True



def printdata(data):
    dashcount = 30
    print('-'*dashcount)
    print('INPUT')
    print('-'*dashcount)
    for row in data:
        print(row)
    print('-'*dashcount)


def formatInput(lines):
    fInput = []

    for l in lines:
        row = []
        for c in l:
            row.append(int(c))
        fInput.append(row)
    return fInput


def main():
    input = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    fInput = formatInput(input)
    start(fInput)


if __name__ == '__main__':
    main()