# #################################
# Day 8
# 
# #################################


def start(data):
    #printdata(data)

    x = 0
    y = 0

    # [y, x]
    # print(data[0][0]) # "top left"
    # print(data[0][4]) # "top right"
    # print(data[4][0]) # "bottom left"
    # print(data[4][4]) # "bottom right"

    scenic_scores = []
    max_y = len(data)
    max_x = len(data[0])

    # data is square, dawg
    for y in range(max_y):
        for x in range(max_x):

            # Check that vertical, up and down
            if y == 0 or y == max_y or x == 0 or x == max_x:
                scenic_scores.append(0)
                continue

            northScore = 0
            southScore = 0
            westScore = 0
            eastScore = 0

            upper_list = [u[x] for u in data[:y+1]]
            upper_list.reverse()
            northScore = lastElementBiggestElement(upper_list)

            lower_list = [d[x] for d in data[y:]]
            southScore = lastElementBiggestElement(lower_list)

            # Check those sides, left and right
            left_list = [l for l in data[y][:x+1]]
            left_list.reverse()
            westScore = lastElementBiggestElement(left_list)

            right_list = [r for r in data[y][x:]]
            eastScore = lastElementBiggestElement(right_list)

            scenic_scores.append(northScore * southScore * westScore * eastScore)

    print(max(scenic_scores))


def lastElementBiggestElement(somelist):
    my_tree_height = somelist[0]
    score = 0
    for elem in somelist[1:]:
        score += 1
        if elem >= my_tree_height:
            return score
    return score



def printdata(data):
    dashcount = 30
    print('-'*dashcount)
    print('INPUT')
    print('-'*dashcount)
    for row in data:
        print(row)
    print('-'*dashcount)


def formatInput(lines):
    fInput = []

    for l in lines:
        row = []
        for c in l:
            row.append(int(c))
        fInput.append(row)
    return fInput


def main():
    input = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    fInput = formatInput(input)
    start(fInput)


if __name__ == '__main__':
    main()