# #################################
# Day 11
# 
# #################################


class Monkey:
    def __init__(self, 
    identifier:int, inspection_func: 'function', evaluation_func: 'function', 
    true_monkey:int, false_monkey:int, 
    starting_items:list[int]) -> None:
        self._identifier = identifier
        self._inspection_func = inspection_func # Must recieve a single int parameter lambda returning int
        self._evaluation_func = evaluation_func # Must recieve a single int parameter lambda returning True or False
        self._true_monkey = true_monkey         # Throw to monkey identifier if evaluation function returns true
        
        self._false_monkey = false_monkey       # Throw to monkey identifier if evaluation function returns false
        self._items = []                        # Collection of items in possession
        self._current_item_in_hand = None       # To contain int values from 'items' list

        self._inspection_counter = 0             # The number of inspection operations performed

        for s in starting_items:
            self._items.append(s)
    

    def get_inspection_count(self):
        return self._inspection_counter

    
    def perform_inspection(self) -> None:
        if len(self._items) == 0:
            # Nothing more to evaluate, move along
            return

        self._inspection_counter += 1

        # This is the inspection that increase the 'worry level'
        item_to_evaluate = self._items.pop(0)
        self._current_item_in_hand = self._inspection_func(item_to_evaluate) 

        # This is the relief that lowers the 'worry level'
        # Permanently fixed to be division by 3, rounded down to the nearest int
        self._current_item_in_hand = int(self._current_item_in_hand / 3)
        return 


    def perform_evaluation(self) -> tuple:
        if self._current_item_in_hand is None:
            # Nothing to evaluate
            # Throw nothing
            return None, None

        evaluation = self._evaluation_func(self._current_item_in_hand)

        if evaluation:
            return self._throw_item_in_hand(self._true_monkey)     
        else:
            return self._throw_item_in_hand(self._false_monkey) 


    def _throw_item_in_hand(self, target_monkey_id: int) -> tuple:
        target_monkey = target_monkey_id
        item = self._current_item_in_hand
        self._current_item_in_hand = None
        return target_monkey, item

    
    def catch_new_item(self, item: int) -> None:
        self._items.append(item)
        return None


def start(monkey_list:list[Monkey]):
    number_of_rounds = 20
    
    for _ in range(number_of_rounds):
        for monkey in monkey_list:
            item_count = len(monkey._items)
            for _ in range(item_count):
                monkey.perform_inspection()
                recipient_monkey, tossed_item = monkey.perform_evaluation()
                if recipient_monkey is None:
                    continue
                
                monkey_list[recipient_monkey].catch_new_item(tossed_item)

    inspections = [i.get_inspection_count() for i in monkey_list]
    inspections.sort()
    print(inspections)
    print(f'Answer: {inspections[-1] * inspections[-2]}')


def main():
    monkeys = [
        Monkey(
            identifier=0,
            inspection_func=lambda e: e * 19,
            evaluation_func=lambda e: e % 7 == 0,
            true_monkey=2,
            false_monkey=3,
            starting_items=[57,58]
        ),
        Monkey(
            identifier=1,
            inspection_func=lambda e: e + 1,
            evaluation_func=lambda e: e % 19 == 0,
            true_monkey=4,
            false_monkey=6,
            starting_items=[66, 52, 59, 79, 94, 73]
        ),
        Monkey(
            identifier=2,
            inspection_func=lambda e: e + 6,
            evaluation_func=lambda e: e % 5 == 0,
            true_monkey=7,
            false_monkey=5,
            starting_items=[80]
        ),
        Monkey(
            identifier=3,
            inspection_func=lambda e: e + 5,
            evaluation_func=lambda e: e % 11 == 0,
            true_monkey=5,
            false_monkey=2,
            starting_items=[82, 81, 68, 66, 71, 83, 75, 97]
        ),
        Monkey(
            identifier=4,
            inspection_func=lambda e: e * e,
            evaluation_func=lambda e: e % 17 == 0,
            true_monkey=0,
            false_monkey=3,
            starting_items=[55, 52, 67, 70, 69, 94, 90]
        ),
        Monkey(
            identifier=5,
            inspection_func=lambda e: e + 7,
            evaluation_func=lambda e: e % 13 == 0,
            true_monkey=1,
            false_monkey=7,
            starting_items=[69, 85, 89, 91]
        ),
        Monkey(
            identifier=6,
            inspection_func=lambda e: e * 7,
            evaluation_func=lambda e: e % 2 == 0,
            true_monkey=0,
            false_monkey=4,
            starting_items=[75, 53, 73, 52, 75]
        ),
        Monkey(
            identifier=7,
            inspection_func=lambda e: e + 2,
            evaluation_func=lambda e: e % 3 == 0,
            true_monkey=1,
            false_monkey=6,
            starting_items=[94, 60, 79]
        )
    ]
    
    start(monkeys)


if __name__ == '__main__':
    main()