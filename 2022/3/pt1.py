# #################################
# Day 3
# ruckpack 
# #################################


def start(data):
    row_number = 0
    common_items = []
    common_items_ints = []
    for i in data:
        row_number += 1
        s1 = set(i[0])
        s2 = set(i[1])
        
        common_value_set = s1 & s2

        if len(common_value_set) == 1:
            common_items.append(common_value_set.pop())
        else:
            print(row_number)
            print(s1)
            print(s2)
            input()

    for i in common_items:
        if ord(i) >= 97:
            # Here it is upper-case
            common_items_ints.append(ord(i) - 96)
        else:
            # Here it is lower-case
            common_items_ints.append(ord(i) - 38)

    #print(common_items_ints)
    print(sum(common_items_ints))


def formatInput(input):
    # Note: it can be assumed that each input row contains an even number of characters.

    fInput = []

    for i in input:
        hinge = int(len(i) / 2 )
        left_half = i[0:hinge]  # up to, not including, hinge
        right_half = i[hinge:]  # hinge to w/e the rest is
        fInput.append([left_half, right_half])

    return fInput


def main():
    input = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    fInput = formatInput(input)
    start(fInput)


if __name__ == '__main__':
    main()