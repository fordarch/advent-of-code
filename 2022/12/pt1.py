# #################################
# Day 12
# 
# #################################


def start(grid, start_yx, stop_yx):
    """
    print(f'start: {start_yx}')
    print(f'goal: {stop_yx}')
    
    for y_idx in range(len(grid)):
        row = []
        for x_idx in range(len(grid[y_idx])):
            row.append(grid[y_idx][x_idx]['value'])
        print(row)
    """

    timeout_loopcount = 10000

    y_val = stop_yx[0]
    x_val = stop_yx[1]
    leaves = []
    leaves.append([y_val, x_val]) # init with the goal coordnates

    while timeout_loopcount > 0:
        if y_val == start_yx[0] and x_val == start_yx[1]:
            # finished the path and made it home
            break
        
        timeout_loopcount -= 1

        y_val, x_val = leaves.pop()

        # -------------------------------
        # NORTH
        # -------------------------------
        flex_idx = y_val -1
        if flex_idx >= 0:
            if grid[flex_idx][x_val]['value'] - 1 >= grid[y_val][x_val]['value']:
                n = grid[flex_idx][x_val]['north']
                e = grid[flex_idx][x_val]['east']
                s = grid[flex_idx][x_val]['south']
                w = grid[flex_idx][x_val]['west']
                grid[flex_idx][x_val]['south']  
                leaves.append([flex_idx, x_val])

        # -------------------------------
        # EAST
        # -------------------------------

        # -------------------------------
        # SOUTH
        # -------------------------------

        # -------------------------------
        # WEST
        # -------------------------------
    

    return


def createGrid(rows):
    grid = []
    impossibly_large_number = 99
    for y_idx in range(len(rows)):
        row = []
        for x_idx in range(len(rows[y_idx])):
            row.append( {'value': rows[y_idx][x_idx], 'north':impossibly_large_number, 'east':impossibly_large_number, 'south':impossibly_large_number, 'west':impossibly_large_number})
        grid.append(row)
    return grid


def findStartAndGoal(rows):
    start_yx = [0,0]
    goal_yx = [0,0]
    
    for y_idx in range(len(rows)):
        for x_idx in range(len(rows[y_idx])):
            if rows[y_idx][x_idx] == -14:
                start_yx = [y_idx, x_idx]
                rows[y_idx][x_idx] = 0
            if rows[y_idx][x_idx] == -28:
                goal_yx = [y_idx, x_idx]
                rows[y_idx][x_idx] = 25

    return rows, start_yx, goal_yx


def formatInput(rows):
    fPuzzleInput = []

    for row in rows:
        fPuzzleInput.append([ord(c)-97 for c in row])
    
    number_grid, start_yx, stop_yx = findStartAndGoal(fPuzzleInput)
    grid = createGrid(number_grid)    

    return grid, start_yx, stop_yx


def main():
    input = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    a,b,c = formatInput(input)
    start(a,b,c)


if __name__ == '__main__':
    main()