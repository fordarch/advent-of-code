# #################################
# Day 5
# Crane of hanoi
# #################################

stacks = {
    1:['H','T','Z','D'],
    2:['Q','R','W','T','G','C','S'],
    3:['P','B','F','Q','N','R','C','H'],
    4:['L','C','N','F','H','Z'],
    5:['G','L','F','Q','S'],
    6:['V','P','W','Z','B','R','C','S'],
    7:['Z','F','J'],
    8:['D','L','V','Z','R','H','Q'],
    9:['B','H','G','N','F','Z','L','D']
}


def start(data):
    my_stacks = stacks.copy()
    
    for command in data:
        for i in range(0, command[0]):
            crate = my_stacks[command[1]].pop()
            my_stacks[command[2]].append(crate)

    print(''.join([my_stacks[k][-1] for k in my_stacks]))



def formatInput(data):
    fData = []

    for i in data:
        s = i.split()
        fData.append([int(s[1]), int(s[3]), int(s[5])])

    return fData


def main():
    data = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            data.append(r)
    fData = formatInput(data)
    start(fData)


if __name__ == '__main__':
    main()