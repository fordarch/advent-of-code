# #################################
# Day 2
# Rock Paper Scissors
# #################################


def start(input):
    total_score = 0
    for i in input:
        res = lookup(i[0], i[1])
        total_score += res
    print(total_score)        


def lookup(firstval, secondval):
    lookup_shape = {
        'A':{'X':'Z', 'Y':'X', 'Z':'Y'},
        'B':{'X':'X', 'Y':'Y', 'Z':'Z'},
        'C':{'X':'Y', 'Y':'Z', 'Z':'X'}        
    }

    lookemup = {
        'A':{'X':3, 'Y':6, 'Z':0},
        'B':{'X':0, 'Y':3, 'Z':6},
        'C':{'X':6, 'Y':0, 'Z':3}
    }

    choice_table = {
        'X':1,
        'Y':2,
        'Z':3
    }

    my_shape = lookup_shape[firstval][secondval]
    win_loss_score = lookemup[firstval][my_shape]
    choice_bonus = choice_table[my_shape]

    return win_loss_score + choice_bonus


def formatInput(input):
    fInput = []

    for i in input:
        fInput.append(i.split())

    return fInput


def main():
    input = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    fInput = formatInput(input)
    start(fInput)


if __name__ == '__main__':
    main()