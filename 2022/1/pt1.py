# #################################
# Day 1
# how many Calories are being carried by the Elf carrying the most Calories.
# #################################


def start(input):
    elves = [0]
    curr_elf_id = 0
    for i in input:
        if i is None:
            curr_elf_id += 1
        else:
            elves.append(0)
            elves[curr_elf_id] += i
    
    maxVal = max(elves)
    
    print(maxVal)        




def formatInput(input):
    fInput = []

    for i in input:
        if len(i) > 0:
            fInput.append(int(i))
        else:
            fInput.append(None)

    return fInput


def main():
    input = []
    with open('./input.txt', 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    fInput = formatInput(input)
    start(fInput)


if __name__ == '__main__':
    main()