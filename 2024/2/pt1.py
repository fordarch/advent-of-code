# #################################
# Day 02
# unusual data from the Red-Nosed reactor
# #################################
YEAR            = '2024'
DAY             = '2'
INPUT_FILE      = 'input.txt'
INPUT_FILE_TEST = 'test_input.txt'


def calculate(number_list):
    """ Input is a list 
        each element of list is a list of ints 
        Returned is count of lists that meet puzzle spec.
    """
    running_total = 0

    for row in number_list:
        if row[0] == 58:
            print('here I am')
        print(f'evaluating row {row}')
        initial_diff = row[1] - row[0]        
        
        if not(abs(initial_diff) == 1 or abs(initial_diff) == 2 or abs(initial_diff) == 3):
            continue  
        
        motif = 1 if initial_diff > 0 else -1 if initial_diff < 0 else 0
        maintained_motif = 1

        for i in range(1, len(row)):
            diff = row[i] - row[i-1]
            
            if abs(diff) == 1 or abs(diff) == 2 or abs(diff) == 3:
                maintained_motif = 1
            else:
                maintained_motif = 0
            
            maintained_motif = 1 if maintained_motif * diff * motif > 0 else 0

            if maintained_motif < 1:
                break
        
        if maintained_motif > 0: 
            print('good!')
            running_total += 1
    
    return running_total


def refine_input(input_1):
    """ Input recieved is a list of strings. 
          [a, b, c]
        Each string is a space-seperated list of strings.
          a := 'a1 a2 a3'
          ...
        Retruend is the list with each string element parsed into a list
          [
            [a1, a2, a3],
            [b1, b2, b3],
            [c1, c2, c3]
          ]
        Also, each element must be cast to int.
    """
    retval = []
    
    for e in input_1:
        retval.append([int(x) for x in e.split(' ')])
    
    return retval


def read_input_file(filename : str) -> list:
    """ Read the target file.
        Store each row into as a new element of a list.
        Return the list of of rows. 
    """
    input = []
    with open(filename, 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    return input 


def main():
    input_file_path = f'{YEAR}/{DAY}/{INPUT_FILE}'
    #input_file_path = f'{YEAR}/{DAY}/{INPUT_FILE_TEST}'

    input_1 = read_input_file(input_file_path)
    input_2 = refine_input(input_1)
    
    puzzle_answer = calculate(input_2)

    print(puzzle_answer)


if __name__ == '__main__':
    main()