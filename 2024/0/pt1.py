# #################################
# Day ##
# DESCRIPTION
# #################################
YEAR            = '2024'
DAY             = '99'
INPUT_FILE      = 'input.txt'
INPUT_FILE_TEST = 'test_input.txt'


def calculate(number_list):
    sum_of_list_elements = sum(number_list)
    return sum_of_list_elements


def read_input_file(filename : str) -> list:
    """ Read the target file.
        Store each row into as a new element of a list.
        Return the list of of rows. 
    """
    input = []
    with open(filename, 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    return input 


def main():
    #input_file_path = f'{YEAR}/{DAY}/{INPUT_FILE}'
    input_file_path = f'{YEAR}/{DAY}/{INPUT_FILE_TEST}'

    input_1 = read_input_file(input_file_path)
    
    total = calculate(input_1)

    print(total)

if __name__ == '__main__':
    main()