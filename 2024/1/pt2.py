# #################################
# Day 1.2
# Search for Historian; compare two lists
# #################################
import re


YEAR            = '2024'
DAY             = '1'
INPUT_FILE      = 'input.txt'
INPUT_FILE_TEST = 'test_input.txt'



def calculate(dict1, dict2):
    """ Perform the calculation as per the instruction spec 
        Recieve two unordered dicts of positive ints.
        Perform calculation between each list and return result.
    """
    rolling_sum = 0

    for e in dict1:
        if e in dict2:
            rolling_sum += e * dict1[e] * dict2[e]
        # Non-intersecting keys, any key in d1 not present in d2, are ignored for calculation.

    return rolling_sum


def split_input_into_two_dicts(input_1):
    """ Split the text input into two dicts of numbers as per 
        instruction spec. 
        
        Output is two dicts (dict1, dict2).
        Each dict is the input (as key) and the number of duplicate occurances (as value)
        e.g. input: 1, 1, 2, 3 
             result: {1:2, 2:1, 3:1}
    """
    dict1 = {}
    dict2 = {}

    for row in input_1:
        splitted = re.split('[ ]+', row)

        d1_value = int(splitted[0])
        d2_value = int(splitted[1])

        if d1_value in dict1:
            dict1[d1_value] += 1
        else:
            dict1[d1_value] = 1

        if d2_value in dict2:
            dict2[d2_value] += 1
        else:
            dict2[d2_value] = 1
    
    return dict1, dict2


def read_input_file(filename : str) -> list:
    """ Read the target file.
        Store each row into as a new element of a list.
        Return the list of of rows. 
    """
    input = []
    with open(filename, 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    return input 


def main():
    input_file_path = './input.txt'
    input_file_path = f'{YEAR}/{DAY}/{INPUT_FILE}'
    #input_file_path = f'{YEAR}/{DAY}/{INPUT_FILE_TEST}'

    input_1 = read_input_file(input_file_path)
    
    dict1, dict2 = split_input_into_two_dicts(input_1)

    total = calculate(dict1, dict2)

    print(total)

if __name__ == '__main__':
    main()