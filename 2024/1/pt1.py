# #################################
# Day 1
# Search for Historian; compare two lists
# #################################
import re


YEAR            = '2024'
DAY             = '1'
INPUT_FILE      = 'input.txt'
INPUT_FILE_TEST = 'test_input.txt'



def calculate(list1, list2):
    """ Perform the calculation as per the instruction spec 
        Recieve two unordered lists of positive ints.
        Sort asc each list.
        Perform calculation between each list and return result.

        Both lists must be assumed to be the same length as per instruction spec. 
    """
    rolling_sum = 0

    list1.sort()
    list2.sort()

    for i in range(len(list1)):
        rolling_sum += abs(list1[i] - list2[i])

    return rolling_sum


def split_input_into_two_lists(input_1):
    """ Split the text input into two lists of numbers as per 
        instruction spec. 
        
        Output is two lists (list1, list2).
        Each list is an unordered list of positive ints e.g. [3, 22, 1, 999]
    """
    list1 = []
    list2 = []

    for row in input_1:
        splitted = re.split('[ ]+', row)
        list1.append(int(splitted[0]))
        list2.append(int(splitted[1]))
    
    return list1, list2


def read_input_file(filename : str) -> list:
    """ Read the target file.
        Store each row into as a new element of a list.
        Return the list of of rows. 
    """
    input = []
    with open(filename, 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    return input 


def main():
    input_file_path = './input.txt'
    input_file_path = f'{YEAR}/{DAY}/{INPUT_FILE}'
    #input_file_path = f'{YEAR}/{DAY}/{INPUT_FILE_TEST}'

    input_1 = read_input_file(input_file_path)
    
    list1, list2 = split_input_into_two_lists(input_1)

    total = calculate(list1, list2)

    print(total)

if __name__ == '__main__':
    main()