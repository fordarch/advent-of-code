# #################################
# Day 5
# seeds! ranges of them!  ---- no solution found here ----
# #################################


def next_map_number(mapint : int, maplist : list) -> int:
    for m in maplist:
        if mapint >= m[1] and mapint < m[1] + m[2]:
            n_dif = abs(mapint - m[1])
            return m[0] + n_dif
                
    # if you're here, no mapping was found
    # we default to the same number that was passed
    return mapint


def read_input_file(filename : str) -> list:
    """ Read the target file.
        Store each row into as a new element of a list.
        Each row is split on spaces and is expected to be a list of ints
        Return the list of of rows. 
    """
    input = []
    with open(filename, 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append([int(x) for x in r.split()]) 

    return input 


def main():
    input_fertilizer_to_water       = '2023/5/input_fertilizer_to_water.txt'
    input_humidity_to_location      = '2023/5/input_humidity_to_location.txt'
    input_light_to_temperature      = '2023/5/input_light_to_temperature.txt'
    input_seed_to_soil              = '2023/5/input_seed_to_soil.txt'
    input_seeds                     = '2023/5/input_seeds.txt'
    input_soil_to_fertilizer        = '2023/5/input_soil_to_fertilizer.txt'
    input_temperature_to_humidity   = '2023/5/input_temperature_to_humidity.txt'
    input_water_to_light            = '2023/5/input_water_to_light.txt'

    map_fertilizer_to_water     = read_input_file(input_fertilizer_to_water)
    map_humidity_to_location    = read_input_file(input_humidity_to_location)
    map_light_to_temperature    = read_input_file(input_light_to_temperature)
    map_seed_to_soil            = read_input_file(input_seed_to_soil)
    seeds                       = read_input_file(input_seeds)
    map_soil_to_fertilizer      = read_input_file(input_soil_to_fertilizer)
    map_temperature_to_humidity = read_input_file(input_temperature_to_humidity)
    map_water_to_light          = read_input_file(input_water_to_light)

    # when we derive a location we store them here. 
    # solution is the minimum int location
    min_location = 999999999

    seed_list = seeds[0]
    for i in range(0, len(seed_list), 2):
        seed = seed_list[i]
        seed_range = seed_list[i + 1]

        for j in range(seed, seed+seed_range-1):
            a = next_map_number(seed, map_seed_to_soil)
            b = next_map_number(a, map_soil_to_fertilizer)
            c = next_map_number(b, map_fertilizer_to_water)
            d = next_map_number(c, map_water_to_light)
            e = next_map_number(d, map_light_to_temperature)
            f = next_map_number(e, map_temperature_to_humidity)
            g = next_map_number(f, map_humidity_to_location)

            if g < min_location:
                min_location = g

    print(min_location)

if __name__ == '__main__':
    main()