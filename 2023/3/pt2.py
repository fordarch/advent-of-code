# #################################
# Day 3 pt2
# broken gondala; sprocket product
# #################################
import re
import numpy as np



def is_it_meaningful(number_to_check : str, row_idx : int, col_idx : int, input_1 : list) -> bool:
    """ We have a number assembled from a 2d list. 
        Look at all the elemnts in the 2d list adjacent to the number and see if a symbol is adjacent (only a match on '*' matters)

        the params we get are the row and column index number that is the same row as the number and the first column after the number

        col idx is the index of the last digit of the number of the row

        return the tuple of the symbol e.g. (row_idx, col_idx)
    """
    column_idx_start = col_idx - (len(number_to_check) - 1)
    column_idx_end = col_idx

    # check above 
    if row_idx > 0:
        the_row_above = input_1[row_idx - 1][column_idx_start:column_idx_end + 1]
        for i in range(0, len(the_row_above)):
            c = the_row_above[i]
            if re.match('[\*]+', c) != None:
                # If this number is adjacent to a '*' character, we have a valid number
                return ((row_idx - 1), (column_idx_start + i))

    # check below 
    if row_idx < len(input_1) - 1:
        the_row_below = input_1[row_idx + 1][column_idx_start:column_idx_end + 1]
        for i in range(0, len(the_row_below)):
            c = the_row_below[i]
            if re.match('[\*]+', c) != None:
                # If this number is adjacent to a '*' character, we have a valid number
                return ((row_idx + 1), (column_idx_start + i))

    # check before
    if column_idx_start > 0:
        c = input_1[row_idx][column_idx_start - 1]
        if re.match('[\*]+', c) != None:
            # If this number is adjacent to a '*' character, we have a valid number
            return (row_idx, (column_idx_start - 1))

    # check after
    if column_idx_end < len(input_1[row_idx]) - 1:
        c = input_1[row_idx][column_idx_end + 1]
        if re.match('[\*]+', c) != None:
            # If this number is adjacent to a '*' character, we have a valid number
            return (row_idx, (column_idx_end + 1))

    # check above-after (upper-right diag)
    if row_idx > 0 and column_idx_end < len(input_1[row_idx]) - 1:
        c = input_1[row_idx - 1][column_idx_end + 1]
        if re.match('[\*]+', c) != None:
            # If this number is adjacent to a '*' character, we have a valid number
            return ((row_idx - 1), (column_idx_end + 1))

    # check upper-left diag
    if row_idx > 0 and column_idx_start > 0: 
        c = input_1[row_idx - 1][column_idx_start - 1]
        if re.match('[\*]+', c) != None:
            # If this number is adjacent to a '*' character, we have a valid number
            return ((row_idx - 1), (column_idx_start - 1))

    # check below-after (lower-right diag)
    if row_idx < len(input_1) - 1 and column_idx_end < len(input_1[row_idx]) - 1: 
        c = input_1[row_idx + 1][column_idx_end + 1]
        if re.match('[\*]+', c) != None:
            # If this number is adjacent to a '*' character, we have a valid number
            return ((row_idx + 1), (column_idx_end + 1))

    # check below-before (lower_left diag)
    if row_idx < len(input_1) - 1 and column_idx_start > 0: 
        c = input_1[row_idx + 1][column_idx_start - 1]
        if re.match('[\*]+', c) != None:
            # If this number is adjacent to a '*' character, we have a valid number
            return ((row_idx + 1), (column_idx_start - 1))
    
    return None


def collect_meaningful_numbers(input_1 : list) -> list:
    """loop ever each element of the 2d array and collect the numbers, retainin ghtem if they are adjacent to a symbol.
        The input of each row is exactly the same; we know this from looking at the input. 
    """

    digit_builder = []

    # key is sprocket (x,y) coordinates; value is list of adjacent numbers
    # {(1,7):[23,84], ...}
    meaningful_numbers = {}

    for row_idx in range(0, len(input_1)):
        for col_idx in range(0, len(input_1[row_idx])):
            current_cell = input_1[row_idx][col_idx]

            response = None
            the_current_cell_is_a_digit = current_cell.isdigit()
            this_cell_is_last_in_row = (col_idx + 1) == len(input_1[row_idx])

            if the_current_cell_is_a_digit:
                digit_builder.append(current_cell)
            
            if this_cell_is_last_in_row and the_current_cell_is_a_digit:
                # the row ends with a number, we complete this number and send it to be tested
                digit_builder_is_not_empty = len(digit_builder) > 0
                if digit_builder_is_not_empty:
                    number_to_check = ''.join(digit_builder)
                    digit_builder = [] # flush digit builder, we already found out current digit to check at this point
                    response = is_it_meaningful(number_to_check, row_idx, col_idx, input_1)
            
            if this_cell_is_last_in_row and not the_current_cell_is_a_digit:
                # the row ends and we end a number; we complete this number and send it to be tested
                digit_builder_is_not_empty = len(digit_builder) > 0
                if digit_builder_is_not_empty:
                    number_to_check = ''.join(digit_builder)
                    digit_builder = [] # flush digit builder, we already found out current digit to check at this point
                    response = is_it_meaningful(number_to_check, row_idx, (col_idx - 1), input_1)
            
            if not the_current_cell_is_a_digit:
                # either ended a number or ended nothing; if we have a number we complete this number and send it to be tested
                digit_builder_is_not_empty = len(digit_builder) > 0
                if digit_builder_is_not_empty:
                    number_to_check = ''.join(digit_builder)
                    digit_builder = [] # flush digit builder, we already found out current digit to check at this point
                    response = is_it_meaningful(number_to_check, row_idx, (col_idx - 1), input_1)
            
            if response is not None:
                if meaningful_numbers.get(response, None) is None:
                    meaningful_numbers[response] = [int(number_to_check)]
                else:
                    meaningful_numbers[response].append(int(number_to_check))
        
    return meaningful_numbers
            

def read_input_file(filename : str) -> list:
    """ Read the target file.
        Store each row into as a new element of a list.
        Return the list of of rows. 
    """
    input = []
    with open(filename, 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    return input 


def main():
    # Read the input file
    input_file_path = './input.txt'
    input_file_path = '2023/3/input.txt'
    #input_file_path = '2023/3/test_input.txt'
    input_1 = read_input_file(input_file_path)
    
    # gather collection of the ints we care about from input
    dict_of_valid_numbers = collect_meaningful_numbers(input_1)

    product_list = []
    for k in dict_of_valid_numbers:
        if len(dict_of_valid_numbers[k]) >= 2:
            xxx = np.prod(dict_of_valid_numbers[k])
            product_list.append(xxx)

    print(sum(product_list))

if __name__ == '__main__':
    main()