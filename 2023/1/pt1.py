# #################################
# Day 1
# What is the sum of all of the [Trebuchet] calibration values?
# #################################

def calculate(number_list):
    sum_of_list_elements = sum(number_list)
    return sum_of_list_elements


def find_first_and_last(input : list) -> list:
    """ Find from each row of the input, the first and 
        last numerical digits. These form a single number.

        Return a list of these each as integers.
    """
    retval = []

    for row in input:
        first_number_in_row = None
        last_number_in_row = None

        for character in row:
            if character.isdigit():
                if first_number_in_row is None:
                    first_number_in_row = character
                last_number_in_row = character

        found_number = f'{first_number_in_row}{last_number_in_row}'
        retval.append(int(found_number))

    return retval


def read_input_file(filename : str) -> list:
    """ Read the target file.
        Store each row into as a new element of a list.
        Return the list of of rows. 
    """
    input = []
    with open(filename, 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    return input 


def main():
    # Read the input file
    input_file_path = './input.txt'
    input_file_path = '2023/1/input.txt'
    input_1 = read_input_file(input_file_path)
    
    # Find first and last numbers in each row
    number_list = find_first_and_last(input_1)

    # Take the sum of all the found numbers
    total = calculate(number_list)

    print(total)

if __name__ == '__main__':
    main()