# #################################
# Day 1.2 
# What is the sum of all of the [Trebuchet] calibration values?
# Target names of numbers
# #################################
import re


#number_names_map = {'one':'1', 'two':'2', 'three':'3', 'four':'4', 'five':'5', 'six':'6', 'seven':'7', 'eight':'8', 'nine':'9'}
number_names_map = {'one':'o1e', 'two':'t2o', 'three':'t3e', 'four':'f4r', 'five':'f5e', 'six':'s6x', 'seven':'s7n', 'eight':'e8t', 'nine':'n9e'}


def calculate(number_list):
    sum_of_list_elements = sum(number_list)
    return sum_of_list_elements


def find_first_and_last(input_2 : list) -> list:
    """ Find from each row of the input, the first and 
        last numerical digits. These form a single number.

        Return a list of these each as integers.
    """
    retval = []

    for row in input_2:
        first_number_in_row = None
        last_number_in_row = None

        for character in row:
            if character.isdigit():
                if first_number_in_row is None:
                    first_number_in_row = character
                last_number_in_row = character

        found_number = f'{first_number_in_row}{last_number_in_row}'
        retval.append(int(found_number))

    return retval


def digit_names_to_ints(input_1 : list) -> list:
    """ In each row may be the name of a digit. 
        Replace the name of each digit (1-9)
        with the actual integer-based value
    """
    new_input = []

    for row in input_1:
        for nnm in number_names_map:
            if nnm in row:
                row = re.sub(nnm, number_names_map[nnm], row)
        new_input.append(row)

    return new_input        


def read_input_file(filename : str) -> list:
    """ Read the target file.
        Store each row into as a new element of a list.
        Return the list of of rows. 
    """
    input = []
    with open(filename, 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    return input 


def main():
    # Read the input file
    input_file_path = './input.txt'
    input_file_path = '2023/1/input.txt'
    #input_file_path = '2023/1/test_input.txt'
    input_1 = read_input_file(input_file_path)

    # modify the input, replacing names of digits with digits
    input_2 = digit_names_to_ints(input_1)
    
    # Find first and last numbers in each row
    number_list = find_first_and_last(input_2)

    # Take the sum of all the found numbers
    total = calculate(number_list)

    print(total)


if __name__ == '__main__':
    main()