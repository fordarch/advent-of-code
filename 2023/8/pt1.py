# #################################
# Day 8
# sand ghosts
# #################################

input_directions = 'LRRRLRRLLRRLRRLRRLRRLRLLRLRLLRRLRLRRRLRRLRRLLRLRLRLRRRLRRRLLRLRRRLLRRRLRLLRRRLLRRLRLRLRRRLLRRLRRRLLRRLRLRRRLLRRRLRRLRLRRRLLRRLRRRLRRLLRRLRRLRRRLRRRLRRRLRRLRRRLLRLRLRLRRRLRRLRRRLRRLRLRRLRLRRRLRRRLRRLRRRLLRRRLLRRLRLRRRLRLRLRRRLRLRLRLRRLRLRRLRRLLRRRLRLLRRLRRRLRRRLLRRLRLLLLRRLRRRR'



def run_path(start_state, terminal_state, direction_map):
    current_state = start_state
    step_counter = 0
    steps_to_take = [a for a in input_directions] 

    while current_state != terminal_state:
        if steps_to_take[step_counter % len(steps_to_take)] == 'L':
            current_state = direction_map[current_state][0]
        else:
            current_state = direction_map[current_state][1]
        step_counter += 1
    
    return step_counter


def make_a_map_out_of_it(input_1):
    # d = {key:[LLL,RRR], ...}
    d = {}

    for row in input_1:
        key = row[0]
        LLL = row[1]
        RRR = row[2]

        d[key] = [LLL,RRR]
    
    return d


def read_input_file(filename : str) -> list:
    input = []

    with open(filename, 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r.split())

    return input 


def main():
    input_file_path = './input.txt'
    input_file_path = '2023/8/input.txt'
    #input_file_path = '2023/7/test_input.txt'
    input_1 = read_input_file(input_file_path)
        
    direction_map = make_a_map_out_of_it(input_1)
    
    steps_to_zzz = run_path('AAA', 'ZZZ', direction_map)
    
    print(steps_to_zzz)


if __name__ == '__main__':
    main()