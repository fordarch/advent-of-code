# #################################
# Day 8
# multiple sand ghosts
# #################################

input_directions = 'LRRRLRRLLRRLRRLRRLRRLRLLRLRLLRRLRLRRRLRRLRRLLRLRLRLRRRLRRRLLRLRRRLLRRRLRLLRRRLLRRLRLRLRRRLLRRLRRRLLRRLRLRRRLLRRRLRRLRLRRRLLRRLRRRLRRLLRRLRRLRRRLRRRLRRRLRRLRRRLLRLRLRLRRRLRRLRRRLRRLRLRRLRLRRRLRRRLRRLRRRLLRRRLLRRLRLRRRLRLRLRRRLRLRLRLRRLRLRRLRRLLRRRLRLLRRLRRRLRRRLLRRLRLLLLRRLRRRR'


def find_answer(durations_to_exit):
    """ YO
    if you are reading this, do not call this method. 
    while it will get the answer, it will take several minutes.
    just cacluate the Least Common Multiple (LCM). There is a nice formula for this. 
    """
    results = []
    mult = 1
    min_val = min(durations_to_exit)

    while True:
        for d in durations_to_exit:
            x = (min_val * mult) % d
            results.append(x)
            
        if max(results) == 0:
            return d * mult
        else:
            results = []
            mult += 1


def run_path(start_state, direction_map):
    current_state = start_state
    step_counter = 0
    steps_to_take = [a for a in input_directions] 

    while current_state[2] != 'Z':
        if steps_to_take[step_counter % len(steps_to_take)] == 'L':
            current_state = direction_map[current_state][0]
        else:
            current_state = direction_map[current_state][1]
        step_counter += 1
    
    return step_counter


def make_a_map_out_of_it(input_1):
    # d = {key:[LLL,RRR], ...}
    d = {}

    for row in input_1:
        key = row[0]
        LLL = row[1]
        RRR = row[2]

        d[key] = [LLL,RRR]
    
    return d


def read_input_file(filename : str) -> list:
    input = []

    with open(filename, 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r.split())

    return input 


def main():
    input_file_path = './input.txt'
    input_file_path = '2023/8/input.txt'
    #input_file_path = '2023/7/test_input.txt'
    input_1 = read_input_file(input_file_path)
        
    direction_map = make_a_map_out_of_it(input_1)
    
    starting_positions = [x for x in direction_map if x[2] == 'A']
    durations_to_exit = []

    for s in starting_positions:
        duration = run_path(s, direction_map)
        durations_to_exit.append(duration)

    answer = find_answer(durations_to_exit)

    print(answer)
    

if __name__ == '__main__':
    main()