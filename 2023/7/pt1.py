# #################################
# Day 7
# camel poker
# #################################



five_of_kind_list       = []
four_of_a_kind_list     = []
full_house_list         = []
three_of_a_kind_list    = []
two_pair_list           = []
one_pair_list           = []
high_card_list          = []

supah_list = []



def calculate():
    total_val = 0

    for i in range(0, len(supah_list)):
        total_val += supah_list[i][1] * (i + 1)
    
    return total_val

def aggregate_buckets():
    """list have been sorted in asc order
        put them together in a supah list
        we want to add the smaller sets first (low value, low index)
    """
    supah_list.extend(high_card_list)    
    supah_list.extend(one_pair_list)  
    supah_list.extend(two_pair_list)     
    supah_list.extend(three_of_a_kind_list) 
    supah_list.extend(full_house_list) 
    supah_list.extend(four_of_a_kind_list)  
    supah_list.extend(five_of_kind_list)                


def sort_buckets():
    """ sort() will sort out lists in asc order"""
    five_of_kind_list.sort()    
    four_of_a_kind_list.sort()     
    full_house_list.sort()         
    three_of_a_kind_list.sort()    
    two_pair_list.sort()           
    one_pair_list.sort()           
    high_card_list.sort()          


def fill_buckets(input_2):
    for row in input_2:
        hand = row[0]
        
        one_set_of_pairs = False
        two_set_of_pairs = False
        high_pair_count = 0

        already_accounted_for = []
        for card in hand:
            n = hand.count(card)
            if n > 1 and (card not in already_accounted_for):
                # check if this is our high pair
                if n > high_pair_count:
                    high_pair_count = n

                # mark that we have a new pair, possibly the second set of pairs
                if one_set_of_pairs:
                    two_set_of_pairs = True
                one_set_of_pairs = True

                already_accounted_for.append(card)
            
        if high_pair_count == 5:
            five_of_kind_list.append(row)
            continue

        if high_pair_count == 4:
            four_of_a_kind_list.append(row)
            continue

        if high_pair_count == 3 and two_set_of_pairs:
            full_house_list.append(row)
            continue

        if high_pair_count == 3 and not two_set_of_pairs:
            three_of_a_kind_list.append(row)
            continue

        if high_pair_count == 2 and two_set_of_pairs:
            two_pair_list.append(row)
            continue

        if high_pair_count == 2 and not two_set_of_pairs:
            one_pair_list.append(row)
            continue
        
        # default; high_pair_count is >=2 if there was any pair
        # no pair means this would be 1, but we don't both setting the value
        high_card_list.append(row)

def pre_fix(input_1):
    """ re-map the cards to ordninal values
    A,K,Q,...,4,3,2 => E,D,C,...,4,3,2
    """
    import re

    for i in range(0, len(input_1)):
        target = input_1[i][0]

        target = re.sub('A','E', target)
        target = re.sub('K','D', target)
        target = re.sub('Q','C', target)
        target = re.sub('J','B', target)
        target = re.sub('T','A', target)

        input_1[i] = (target, input_1[i][1])
    
    return input_1
        

def read_input_file(filename : str) -> list:
    """ Read the target file.
        Store each row into as a new element of a list.
        Return the list of of rows. 
    """
    input = []
    with open(filename, 'r') as infile:
        for row in infile:
            r = row.rstrip()
            k,v = r.split()
            input.append((k,int(v)))
    return input 


def main():
    input_file_path = './input.txt'
    input_file_path = '2023/7/input.txt'
    #input_file_path = '2023/7/test_input.txt'
    input_1 = read_input_file(input_file_path)

    input_2 = pre_fix(input_1)

    fill_buckets(input_2)
    sort_buckets()
    aggregate_buckets()
    spider_man = calculate()

    print(spider_man)

if __name__ == '__main__':
    main()