# #################################
# Day 5
# sand boat race!
# #################################
import numpy as np


def reflect_a(a, d):
    pivot = d/2

    b = pivot + (pivot - a) 

    return int(b)


def polypress(p, d):
    # given a press time and maximum duration, d, of the race
    # calculate the distance traveled
    return (p) * (d - p)


def binary_search_for_the_red_october(d, b):
    """ find the number of seconds to (p)ress the button
        such that the resultant distance traveled in the provided time is either 
        equal to the target distance or the first integer that results less than the target distance.  
    """
    low = 0
    high = int(d/2) + 1
    p = int(((high + low) / 2))
    t = polypress(p, d)

    while (t != b):
        if (high - low) == 1 and (low == p):
            # you found it! kinda. 
            # you found the first integer that results in a value less than the target time. 
            break

        if t > b:
            high = p 
            p = int( (low + high) / 2 )
        if t < b:
            low = p
            p = int( ((low + high) / 2) + 1 )
        if t == b:
            # you found it!
            break
        if low == high:
            print('It was at this moment he knew he fucked up.')
            return -1

        t = polypress(p, d)
    
    return p
     


def figure_it_out(input_1):
    time_list = input_1[0]
    distance_list = input_1[1]

    n_ways_to_win_list = []

    # match the number to find p
    # p about d/2 gives the min and max values that tie b
    # we'll call the min and max p, (a)lpha and (b)eta 
    # then we will take the max - min - 1 to provide the solution

    for i in range(0, len(time_list)):
        this_race_time_to_match = distance_list[i]
        this_race_time_provided = time_list[i]
        

        a = binary_search_for_the_red_october(this_race_time_provided, this_race_time_to_match)
        b = reflect_a(a, this_race_time_provided)

        n_ways_to_win_list.append(b - a - 1)        

    return np.prod(n_ways_to_win_list)


def read_input_file(filename : str) -> list:
    """ Read the target file.
        Input file like
            Time:        46     82     84     79
            Distance:   347   1522   1406   1471

        Result like 
            [
                [1,2,3],
                [7,8,9]
            ]
    """
    input = []
    with open(filename, 'r') as infile:
        for row in infile:
            r = row.rstrip().split()
            input.append([int(x) for x in r])
    return input 


def main():
    # Read the input file
    input_file_path = './input.txt'
    input_file_path = '2023/6/input.txt'
    #input_file_path = '2023/6/test_input.txt'
    input_1 = read_input_file(input_file_path)

    answer = figure_it_out(input_1)

    print(answer)



if __name__ == '__main__':
    main()