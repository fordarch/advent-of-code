# #################################
# Day 4
# scratch-off tickets
# #################################
import re


def calculate(input_2):
    """ input 2 is a list of tuples
        each tuple is two lists
        list 0 is the winner numbers
        list 1 is the game numbers (the ticket to evaluate)
    """
    # {card1:1, card2:20, card3:0}
    scratch_card_dict = {}

    # We have at least the original scratch cards
    for i in range(0, len(input_2)):
        scratch_card_dict[i] = 1

    # start looping over the scratch cards and finding winners
    for i in range(0, len(input_2)):
        times_were_looping_this_card = scratch_card_dict[i]

        while times_were_looping_this_card > 0:

            matches_for_this_card = 0

            for n in input_2[i][1]:
                if n in input_2[i][0]:
                    matches_for_this_card += 1
            
            if matches_for_this_card > 0:
                for j in range(1, matches_for_this_card + 1):
                    scratch_card_dict[i + j] += 1
            
            times_were_looping_this_card -= 1
        
    values = sum(scratch_card_dict.values())

    return values




def split_inputs(input_1 : list) -> list:
    """ return two lists of numbers from the format of the input
    """
    list_of_tuples = []
    winner_list = []
    game_list = []

    for row in input_1:
        split_in_two = row.split('|')
        clean_left = re.sub('^Card [0-9]+:[ ]+', '', split_in_two[0])
        cleaner_left = re.sub('[ ]+', ' ', clean_left)
        winner_list = cleaner_left.rstrip().split(' ')

        clean_game_list = re.sub('[ ]+', ' ', split_in_two[1])
        game_list = clean_game_list.lstrip().split(' ')
        list_of_tuples.append( (winner_list, game_list) )
    
    return list_of_tuples


def read_input_file(filename : str) -> list:
    """ Read the target file.
        Store each row into as a new element of a list.
        Return the list of of rows. 
    """
    input = []
    with open(filename, 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    return input 


def main():
    # Read the input file
    input_file_path = './input.txt'
    input_file_path = '2023/4/input.txt'
    #input_file_path = '2023/4/test_input.txt'

    input_1 = read_input_file(input_file_path)
    input_2 = split_inputs(input_1)

    answer = calculate(input_2)

    print(answer)



if __name__ == '__main__':
    main()