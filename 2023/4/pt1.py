# #################################
# Day 4
# scratch-off tickets
# #################################
import re


def calculate(input_2):
    """ input 2 is a list of tuples
        each tuple is two lists
        list 0 is the winner numbers
        list 1 is the game numbers (the ticket to evaluate)
    """
    
    total_game_points_accumulated = 0

    for row in input_2:
        matches_for_this_card = 0

        for n in row[1]:
            if n in row[0]:
                matches_for_this_card += 1
        
        if matches_for_this_card > 0:
            total_game_points_accumulated += 2 ** (matches_for_this_card - 1)
            

    return total_game_points_accumulated



def split_inputs(input_1 : list) -> list:
    """ return two lists of numbers from the format of the input
    """
    list_of_tuples = []
    winner_list = []
    game_list = []

    for row in input_1:
        split_in_two = row.split('|')
        clean_left = re.sub('^Card [0-9]+:[ ]+', '', split_in_two[0])
        cleaner_left = re.sub('[ ]+', ' ', clean_left)
        winner_list = cleaner_left.rstrip().split(' ')

        clean_game_list = re.sub('[ ]+', ' ', split_in_two[1])
        game_list = clean_game_list.lstrip().split(' ')
        list_of_tuples.append( (winner_list, game_list) )
    
    return list_of_tuples


def read_input_file(filename : str) -> list:
    """ Read the target file.
        Store each row into as a new element of a list.
        Return the list of of rows. 
    """
    input = []
    with open(filename, 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    return input 


def main():
    # Read the input file
    input_file_path = './input.txt'
    input_file_path = '2023/4/input.txt'
    input_file_path = '2023/4/test_input.txt'
    input_1 = read_input_file(input_file_path)

    input_2 = split_inputs(input_1)

    answer = calculate(input_2)
    print(answer)



if __name__ == '__main__':
    main()