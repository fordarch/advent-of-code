# #################################
# Day 2
# dice game, min set, sum of 'powers' of each game
# #################################
import re


def calculate(input_2):
    """Each row is the row of the input file. 
        Each row is a list of dictionary elements containing the fields RBG.
        Look at each element of each row and evaluate if it meets the crieria
        """
    min_power_each_game = []

    min_red = 1
    min_blue = 1
    min_green = 1
    
    for row in input_2:
        min_red = max([int(v['red']) for v in row])
        min_blue = max([int(v['blue']) for v in row])
        min_green = max([int(v['green']) for v in row])

        new_entry = min_red * min_blue * min_green

        min_power_each_game.append(new_entry)
    
    return min_power_each_game



def structure_input(input_1 : list) -> list:
    """ Parse out the 'human readable' bits and structure the input 
        into something more actionable. 

        this order matters ~RED, BLUE, GREEN~
        returns [{id1:[{r,b,g}, ..., {r,b,g}]}, {id2:[r,b,g]}]

        Turn the input, each row, into a list of dictionary values such that 
        each row is marked by the id of the row and the value is a list of dictionaries that each represent each input of rbg counts. 
    """
    retval = []

    for row in input_1:
        better_row = []
        elements_of_row = row.split(';')
        

        for cubes in elements_of_row:
            subset = {'red':0, 'blue':0, 'green':0}
            all_but_first_bit = re.sub('Game [0-9]+: ', '', cubes)
            elements = all_but_first_bit.split(',')
            for e in elements:
                if 'red' in e:
                    subset['red'] = re.search('[0-9]+', e).group()
                
                if 'blue' in e:
                    subset['blue'] = re.search('[0-9]+', e).group()
                
                if 'green' in e:
                    subset['green'] = re.search('[0-9]+', e).group()
            better_row.append(subset)
        
        retval.append(better_row)

    return retval


def read_input_file(filename : str) -> list:
    """ Read the target file.
        Store each row into as a new element of a list.
        Return the list of of rows. 
    """
    input = []
    with open(filename, 'r') as infile:
        for row in infile:
            r = row.rstrip()
            input.append(r)
    return input 


def main():
    # Read the input file
    input_file_path = './input.txt'
    input_file_path = '2023/2/input.txt'
    input_1 = read_input_file(input_file_path)
    
    # Turn this shitty input into something actionable
    input_2 = structure_input(input_1)

    # Return the list of rows that match the criteria
    list_of_rows_idx_starting_with_one = calculate(input_2)

    print(sum(list_of_rows_idx_starting_with_one))

if __name__ == '__main__':
    main()